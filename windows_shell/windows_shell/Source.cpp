#include "Helper.h"
#include <Windows.h>

#define MAX_PATH 2048

using namespace std;

string Command(string command, vector<string> args);
void listdir(const char *path);

int CallMyDLL(void);


void main()
{
	Helper *helper = new Helper();
	string command;
	vector<string> words;
	while (true)
	{
		cout << ">>";
		getline(cin, command);
		words = helper->get_words(command);
		if (words.size() != 0)
		{
			cout << Command(words[0], words) << "\n";
		}
	}
}


string Command(string command, vector<string> args)
{
	if (command == "pwd")
	{
		char buffer[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, buffer);
		return buffer;
	}
	if (command == "cd")
	{
		SetCurrentDirectory(args[1].c_str());
		char buffer[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, buffer);
		return buffer;
	}
	if (command == "create")
	{
		ofstream newFile(args[1]);
		return "File " + args[1] + " Created!";
	}
	if (command == "ls")
	{
		char buffer[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, buffer);
		listdir(buffer);
	}
	if (command == "secret")
	{
		return to_string(CallMyDLL());
	}
	if (command == "")
	{
		return "";
	}
	return "Sorry there is no command such as " + command;
}

void listdir(const char *path)
{
	DIR *pdir = NULL;
	pdir = opendir(path);
	struct dirent *pent = NULL;
	if (pdir == NULL)
	{
		printf("\nERROR! pdir could not be initialised correctly");
		return;
	}
	while (pent = readdir(pdir))
	{
		if (pent == NULL)
		{
			printf("\nERROR! pent could not be initialised correctly");
			return;
		}
		printf("%s\n", pent->d_name);
	}
	closedir(pdir);

}

int CallMyDLL(void)
{
	/* get handle to dll */
	HINSTANCE hGetProcIDDLL = LoadLibrary("secret.dll");

	/* get pointer to the function in the dll*/
	FARPROC lpfnGetProcessID = GetProcAddress(HMODULE(hGetProcIDDLL), "TheAnswerToLifeTheUniverseAndEverything");

	/*
	Define the Function in the DLL for reuse. This is just prototyping the dll's function.
	A mock of it. Use "stdcall" for maximum compatibility.
	*/
	typedef int(__stdcall * pICFUNC)();

	pICFUNC MyFunction;
	MyFunction = pICFUNC(lpfnGetProcessID);

	/* The actual call to the function contained in the dll */
	int intMyReturnVal = MyFunction();

	/* Release the Dll */
	FreeLibrary(hGetProcIDDLL);

	/* The return val from the dll */
	return intMyReturnVal;
}