﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex19
{
    public partial class Form2 : Form
    {
        public Form2(string user)
        {
            InitializeComponent();
            User = user;
        }

        string User;
        string[] data;
        private void Form2_Load(object sender, EventArgs e)
        {
            if (File.Exists(User + "BD.txt"))
            {
                data = File.ReadAllLines(User + "BD.txt");
            }
            else
            {
                File.WriteAllText(User + "BD.txt","");
            }
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            label2.Text = "אין יומולדת לאף אחד בתאריך זה";
            foreach (string line in data)
            {
                string[] args = line.Split(',');
                if (args[1] == monthCalendar1.SelectionRange.Start.ToShortDateString())
                {
                    label2.Text = "בתאריך זה יש ל" + args[0] + " יומולדת";
                    break;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            bool ok = true;
            foreach (string line in data)
            {
                string[] args = line.Split(',');
                if (args[1] == dateTimePicker1.Value.ToShortDateString())
                {
                    ok = false;
                    break;
                }
            }

            if (ok == true)
            {
                string oldData = File.ReadAllText(User + "BD.txt");
                File.WriteAllText(User + "BD.txt", oldData + "\r\n" + textBox1.Text + "," + dateTimePicker1.Value.ToShortDateString());
                data = File.ReadAllLines(User + "BD.txt");
                dateTimePicker1.ResetText();
                textBox1.Text = "";
                MessageBox.Show("הנתונים נוספו בהצלחה");
            }
            else
            {
                MessageBox.Show("יש יומולדת בתאריך זה");
            }
        }
    }
}
