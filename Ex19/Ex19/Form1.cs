﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex19
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool login = false;
            foreach (string line in File.ReadAllLines("Users.txt"))
            {
                string [] data = line.Split(',');
                if (data[0] == textBox1.Text && data[1] == textBox2.Text)
                {
                    login = true;
                    break;
                }
            }
            if (login == true)
            {
                Form2 form = new Form2(textBox1.Text);
                form.Show();
                this.Hide();
                form.Closed += (s, args) => this.Close();
            }
            else
            {
                MessageBox.Show("שם משתמש או סיסמה שגויים");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
